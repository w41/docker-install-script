#!/bin/bash
#Update
sudo apt-get update
sudo apt-get dist-upgrade -y

# docker requires apt-transport-https
sudo apt-get install apt-transport-https -y

# now setup the apt repository
sudo apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo echo "deb https://apt.dockerproject.org/repo debian-jessie main" | sudo tee /etc/apt/sources.list.d/docker.list

# Finally install the package.
sudo apt-get update
sudo apt-get install docker-engine -y

# Start the docker service
sudo service docker start

# Add ourselves to the docker group so that we 
# can use 'docker' commands without sudo.
sudo adduser $USER docker

#install docker compose
echo "DOCKER COMPOSE install"

sudo apt-get install python-pip -y
sudo pip install --upgrade pip
sudo pip install docker-compose
sudo apt-get install python-pip -y
sudo pip install --upgrade pip
sudo pip install docker-compose

